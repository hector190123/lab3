import 'dart:async';

class TextStream {

  Stream<String> getText() async* {
    final List<String> texts = [ "Hello whatsup !", "Bro is very handsome", "hhahaa", "whatsup", "Bún bò huế"];

    yield* Stream.periodic(Duration(seconds: 5), (int t) {
      int index = t % 5;
      return texts[index];
    });
  }
}
